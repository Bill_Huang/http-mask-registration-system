/*
 * Database.cpp
 *
 *  Created on: Apr 10, 2020
 *      Author: bill
 */

#include "../Include/Database.hpp"
#include <Sys/Include/SysDef.hpp>
#include <Eden/Include/Gcd/Gcd.hpp>
#include <map>
#include <queue>
#include <iostream>
#include <mutex>
#include <mysql/my_global.h>
#include <mysql/mysql.h>
#include <thread>



namespace DB {

	mutex Mutex_;

	const _UINT MAX_NUM_SQL_STATEMENTS = 1024;

//	const time_t DURATION = (2*7*24*60*60);
	const time_t DURATION = (1*1*1*1*60);
	const char *HOST_NAME = "jp";
	const char *DATABASE_NAME = "mask";
	const char *USER = "mask";
	const char *PASSWORD = "maskpassword";

	 MYSQL *MysqlHandle_ = nullptr;

	 bool flagShutdown_=false;
	 bool flagShutdownOk_ = false;

	mutex WriteQueueMutex_;
	struct WRITE_ENTRY {
		enum ACTION {
			Null=0,
			Insert,
			Update
		} Action_;
		_STRING Id_;
		_STRING Phone_;
		SYS::BIRTHDAY Birthday_;
		_WORD Distributor_;
		time_t Time_;
		WRITE_ENTRY() :Action_(Null),Distributor_(0),Time_(0) {}
		WRITE_ENTRY(ACTION a,_STRING const &u_id,_STRING const &phone,SYS::BIRTHDAY b,_WORD dist,time_t t)
			: Action_(a),Id_(u_id),Phone_(phone),Birthday_(b),Distributor_(dist),Time_(t) {}
	};
	queue<WRITE_ENTRY> WriteQueue;

	struct REC_INDEX {
		time_t LastTime_;
		REC_INDEX(time_t t) : LastTime_(t) {}
	};
	map<_STRING,REC_INDEX> Index_;



	void _Proc_DatabaseAction() {
		WRITE_ENTRY Entry;
		_STRING SqlStatement;
		_CHAR Buf[1024];
		_UINT Count=0;

		WriteQueueMutex_.lock();
		while ( !WriteQueue.empty() && Count < MAX_NUM_SQL_STATEMENTS) {
			Entry = WriteQueue.front();
			WriteQueue.pop();
			switch (Entry.Action_) {
			case WRITE_ENTRY::Insert:
				sprintf(
					Buf,
					"INSERT INTO user_info VALUES('%s','%s','%04d-%02d-%02d',%d,%d);",
					Entry.Id_.c_str(),
					Entry.Phone_.c_str(),
					Entry.Birthday_.Year_,
					Entry.Birthday_.Month_,
					Entry.Birthday_.Day_,
					Entry.Distributor_,
					(int)Entry.Time_
					);
				break;
			case WRITE_ENTRY::Update:
				sprintf(
					Buf,
					"UPDATE user_info SET req_time=%d WHERE user_id='%s';",
					(int)Entry.Time_,
					Entry.Id_.c_str()
					);
				break;
			default:
				_XASSERT(0); break;
			}
			SqlStatement+=Buf;
			++Count;

		}
		WriteQueueMutex_.unlock();

		int Ret = mysql_real_query(MysqlHandle_,SqlStatement.c_str(),SqlStatement.length());
		if ( Ret != 0 ) {
			_XASSERT(0);
		}


		int Status;
		do {
		  auto Result = mysql_store_result(MysqlHandle_);
		  if (Result) {
		    mysql_free_result(Result);
		  }
		  if ((Status = mysql_next_result(MysqlHandle_)) > 0) {
			  _XASSERT(0);
		  }
		} while (Status == 0);

	}



	void DatabaseAction() {
		_UINT QueueSize;
		bool flagExit_ = false;
		while ( flagExit_==false ) {
			WriteQueueMutex_.lock();
			QueueSize = WriteQueue.size();
			WriteQueueMutex_.unlock();
			if ( QueueSize == 0 ) {
				if ( flagShutdown_ ) {
					flagExit_ = true;
				}
				else {
					this_thread::yield();
					usleep(10*1000);
				}
			}
			else {
				_Proc_DatabaseAction();
			}
		}
		flagShutdownOk_ = true;
	}



	void Record_Update(_STRING const &id_str,_STRING const &str_phone,SYS::BIRTHDAY birthday,_WORD dist,time_t t) {
		WriteQueueMutex_.lock();
		WriteQueue.push(WRITE_ENTRY(WRITE_ENTRY::Update,id_str,str_phone,birthday,dist,t));
		WriteQueueMutex_.unlock();
	}



	void Record_Append(_STRING const &id_str,_STRING const &str_phone,SYS::BIRTHDAY birthday,_WORD dist,time_t t) {
		WriteQueueMutex_.lock();
		WriteQueue.push(WRITE_ENTRY(WRITE_ENTRY::Insert,id_str,str_phone,birthday,dist,t));
		WriteQueueMutex_.unlock();
	}



	TEST_RET Test(_STRING const &id_str,_STRING const &str_phone,SYS::BIRTHDAY birthday,_WORD dist) {
		lock_guard<mutex> Lock(Mutex_);
		auto Iter = Index_.find(id_str);
		time_t Now=time(NULL);
		if ( Iter != Index_.end() ) { // Found
			if ( (Now - Iter->second.LastTime_) > DURATION ) { // Accept
				Iter->second.LastTime_ = Now;
				Record_Update(id_str,str_phone,birthday,dist,Now);
				return TEST_RET(TEST_RET::Accept,Iter->second.LastTime_);
			}
			else { // Reject
				return TEST_RET(TEST_RET::Reject,Iter->second.LastTime_);
			}
		}
		else { // Not found in Index_
			Index_.insert(pair<_STRING,REC_INDEX>(id_str,Now) );
			Record_Append(id_str,str_phone,birthday,dist,Now);
			return TEST_RET(TEST_RET::Accept,Now);
		}
	}



	bool Initialize() {
		MysqlHandle_ = mysql_init(NULL);
		_XASSERT(MysqlHandle_);
		if ( !mysql_real_connect(MysqlHandle_,HOST_NAME,USER,PASSWORD,DATABASE_NAME, 0, NULL, CLIENT_MULTI_STATEMENTS) ) {
			_XASSERT(0);
		}

		if ( mysql_query(MysqlHandle_, "SELECT user_id,req_time FROM user_info")) {
			_XASSERT(0);
		}
		MYSQL_RES *result = mysql_store_result(MysqlHandle_);
		if (result == NULL) {
			_XASSERT(0);
		}
		MYSQL_ROW Row;
		while ((Row = mysql_fetch_row(result))) {
			_STRING strId = Row[0];
			time_t Time = atoi(Row[1]);
			_XASSERT(Index_.find(strId) == Index_.end());
			Index_.insert( pair<_STRING,REC_INDEX>(strId,REC_INDEX(Time)) );
		}
		mysql_free_result(result);

		flagShutdown_=false;
		flagShutdownOk_=false;
		GCD::Dispatch(DatabaseAction,GCD::SERIAL);

		return true;
	}



	void Shutdown(){
		flagShutdown_ = true;
		while ( flagShutdownOk_==false ) {
			this_thread::yield();
		}
		mysql_close(MysqlHandle_);
	}
};
