/*
 * Database.hpp
 *
 *  Created on: Apr 10, 2020
 *      Author: bill
 */

#pragma once
#ifndef SOURCE_DATABASE_HPP_
#define SOURCE_DATABASE_HPP_

#include "./SysDef.hpp"

namespace DB {

	struct TEST_RET {
		enum ENUM_STATE {
			Accept=0,
			Reject=1,
		} State_;
		time_t LastTime_;
		TEST_RET(ENUM_STATE s, time_t last_req_timept) : State_(s),LastTime_(last_req_timept) {}
	};

	TEST_RET Test(_STRING const &id_str,_STRING const &str_phone,SYS::BIRTHDAY birthday,_WORD dist);

	bool Initialize();
	void Shutdown();
};


#endif /* SOURCE_DATABASE_HPP_ */
