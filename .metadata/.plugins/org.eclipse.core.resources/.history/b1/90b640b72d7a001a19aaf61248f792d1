/*
 * SysDef.cpp
 *
 *  Created on: Mar 20, 2020
 *      Author: bill
 */

#include "../Include/SysDef.hpp"
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <string.h>
#include <iostream>
#include <b64/encode.h>
#include <b64/decode.h>
#include <sstream>
#include <mutex>



namespace SYS {

	const _INT CIPHER_PADDING_SIZE = 64;

	const _UINT16 HTTP_Port_NO = 80;
	const _INT HTTP_NUM_THREADS = 32;

	const filesystem::path PATH_DATABASE("/opt/data/MaskRequest.db");

	const _UINT16 UDP_Port_RANGE_BEGIN = 10240;
	const _UINT16 UDP_Port_RANGE_END = UDP_Port_RANGE_BEGIN+10-1;



//	_BYTE *Key_ = (_BYTE *)"a1234567890bcdef0132457689abcdef";
	_BYTE *Key_ = (_BYTE *)"ewiuf73nx4094c89d0sxm1[0[234cn09q3";
//	_BYTE *InitialVector_ = (_BYTE *)"0123456789abcdef";
	_BYTE *InitialVector_ = (_BYTE *)"ciuowef0hj0f9umz3";




	mutex Mutex_;
	base64::encoder Base64Encoder;
	base64::decoder Base64Decoder;



#pragma pack(push,1)
	struct PACKET {
		MASK_REQUEST Request_;
		_WORD Digest_;
	};
#pragma pack(pop)



	_WORD Fletcher16(_BUF &buf )	{
		_BYTE *Ptr = (_BYTE *)buf.Ptr();
		size_t Count= buf.DataLen();
		_WORD Sum1 = 0;
		_WORD Sum2 = 0;
		while ( Count ) {
			Sum1 = (Sum1 + (*Ptr)) & 0xff;
	      	Sum2 = (Sum2 + Sum1) & 0xff;
	      	--Count;
	      	++Ptr;
			}
		return (Sum2 << 8) | Sum1;
	}



	int _Encrypt(_BYTE *src_ptr, int src_len, _BYTE *output_ptr)
	{
	    EVP_CIPHER_CTX *CipherContext;

	    int Ret;
	    int Len;
	    int OutputLen;

	    CipherContext = EVP_CIPHER_CTX_new();

	    Ret=EVP_EncryptInit_ex(CipherContext, EVP_aes_256_cbc(), NULL, Key_, InitialVector_);
	    _XASSERT(Ret==1);

	    Ret=EVP_EncryptUpdate(CipherContext, output_ptr, &Len, src_ptr, src_len);
	    _XASSERT(Ret==1);

	    OutputLen = Len;

	    Ret=EVP_EncryptFinal_ex(CipherContext, output_ptr + Len, &Len);
	    _XASSERT(Ret==1);

	    OutputLen += Len;

	    EVP_CIPHER_CTX_free(CipherContext);

	    return OutputLen;
	}



	int _Decrypt(unsigned char *ciphertext, int ciphertext_len, unsigned char *src_ptr)
	{
	    EVP_CIPHER_CTX *CipherContext;

	    int Ret;
	    int Len;
	    int OutputLen;

	    CipherContext = EVP_CIPHER_CTX_new();

	    Ret=EVP_DecryptInit_ex(CipherContext, EVP_aes_256_cbc(), NULL, Key_, InitialVector_);
	    if (Ret!=1) {
	    	return 0;
	    }

	    Ret=EVP_DecryptUpdate(CipherContext, src_ptr, &Len, ciphertext, ciphertext_len);
	    if (Ret!=1) {
	    	return 0;
	    }

	    OutputLen = Len;
	    Ret=EVP_DecryptFinal_ex(CipherContext, src_ptr + Len, &Len);
	    if (Ret!=1) {
	    	return 0;
	    }

	    OutputLen += Len;

	    EVP_CIPHER_CTX_free(CipherContext);

	    return OutputLen;
	}



	_OBJ<IMem> Encrypt(_OBJ<IMem> mem) {
		// Symmetric encryption, AES
	    _BYTE *EncryptBuf = new _BYTE[mem->DataLen() + CIPHER_PADDING_SIZE];
	    int EncryptedDataLen = _Encrypt ((_BYTE *)mem->Ptr(), mem->DataLen(),EncryptBuf);
	    _XASSERT(EncryptedDataLen <= (int)mem->DataLen() + CIPHER_PADDING_SIZE);
	    _OBJ<IMem> EncrypedData = _MEM::New(EncryptedDataLen,EncryptBuf);
	    delete EncryptBuf;
	    return EncrypedData;
	}



	_OBJ<IMem> Decrypt(_OBJ<IMem> mem) {
		_OBJ<IMem> Ret;
		_BYTE *DecrytBuf = new _BYTE[mem->DataLen()];
	    _UINT DecrytedDataLen = _Decrypt((_BYTE *)mem->Ptr(), mem->DataLen(), DecrytBuf);
	    if ( DecrytedDataLen ) {
	    	Ret = _MEM::New(DecrytedDataLen,DecrytBuf);
	    }
	    delete DecrytBuf;
	    return Ret;
	}



	_OBJ<IMem> MASK_REQUEST::Serialize() {
		_OBJ<IMem> Data = _MEM::New(sizeof(PACKET));
		Data->DataLen(sizeof(PACKET));
		PACKET *pPacket = (PACKET *)Data->Ptr();
		pPacket->Request_ = *this;
		_BUF DigestBuf(this,sizeof(MASK_REQUEST));
		pPacket->Digest_ = Fletcher16(DigestBuf);

		_OBJ<IMem> EncryptedData = Encrypt(Data);
		return EncryptedData;
	}



	void MASK_REQUEST::Id(_STRING const &str) {
		_XASSERT(str.length() <= MAX_ID_LEN );
		strcpy(Id_,str.c_str());
	}



	void MASK_REQUEST::RandomGenerate(_UINT mod) {
		_UINT Number = rand() % mod;
		sprintf(Id_,"A%09d",Number);
	}



	bool CheckDigest(PACKET *packet_ptr) {
		_BUF DigestBuf(&(packet_ptr->Request_),sizeof(MASK_REQUEST));
		return (packet_ptr->Digest_ == Fletcher16(DigestBuf) )?true:false;
	}



	static int IdValTab_[26] = {10,11,12,13,14,15,16,17,34,18,19,20,21,22,35,23,24,25,26,27,28,29,32,30,31,33 };
	bool CheckId(PACKET *packet_ptr) {
		_CHAR *pStr=packet_ptr->Request_.Id_;
		_CHAR Head = pStr[0];
		if ( Head < 'A' || Head > 'Z' ) {
			return false;
		}
		_CHAR Gender = pStr[1];
		if ( Gender != '1' || Gender != '2' ) {
			return false;
		}
        int Sum = IdValTab_[pStr[0] - 'A'] / 10 + (IdValTab_[pStr[0] - 'A'] % 10) * 9 + (pStr[8] - '0') + (pStr[9] - '0');
        for (int i = 1; i < 8; i++)
        	Sum += (pStr[i] - '0') * (9 - i);
        if (Sum % 10 != 0) {
        	return false;
        }
        else {
        	return true;
        }
		return true;
	}



	bool Deserialize(MASK_REQUEST *ret,_OBJ<IMem> mem) {
		_OBJ<IMem> DecrypedMem=Decrypt(mem);
		if ( !DecrypedMem ) {
			return false;
		}
		PACKET *pPacket = (PACKET *)DecrypedMem->Ptr();
		if ( !CheckDigest(pPacket) || !CheckId(pPacket)) {
			return false;
		}
		*ret = pPacket->Request_;
		return true;
	}



	_STRING Base64_Encode(_STRING const &src) {
		lock_guard<mutex> Lock(Mutex_);
		stringstream StreamSource(src);
		stringstream Base64Stream;
		base64::encoder Encoder;
		Encoder.encode(StreamSource,Base64Stream);
		_STRING Ret(Base64Stream.str());
		// URL Compatible
		auto Iter = Ret.begin();
		while (Iter != Ret.end() ) {
			if ( *Iter == '+' ) { *Iter = '-'; }
			if ( *Iter == '/' ) { *Iter = '_'; }
			if ( *Iter == '=' ) { *Iter = '.'; }
			++Iter;
		}
		return Ret;

	}



	_STRING Base64_Decode(_STRING const &src) {
		lock_guard<mutex> Lock(Mutex_);
		// URL Compatible
		_STRING EncodedStr(src);
		auto Iter = EncodedStr.begin();
		while (Iter != EncodedStr.end() ) {
			if ( *Iter == '-' ) { *Iter = '+'; }
			if ( *Iter == '_' ) { *Iter = '/'; }
			if ( *Iter == '.' ) { *Iter = '='; }
			++Iter;
		}
		stringstream Base64Stream(src);
		stringstream DecodedStream;
		base64::decoder Decoder;
		Decoder.decode(Base64Stream,DecodedStream);
		return DecodedStream.str();
	}



	void Initialize() {
		// NULL
	}



	void Shutdown() {
		// NULL
	}

};


