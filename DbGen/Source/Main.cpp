/*
 * Main.cpp
 *
 *  Created on: Mar 19, 2020
 *      Author: bill
 */

#include <Sys/Include/SysDef.hpp>
#include <Sys/Include/Database.hpp>
#include <Eden/Include/Gcd/Gcd.hpp>
#include <Eden/Include/Core/CoreMem.hpp>
#include <cstring>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <signal.h>
#include <iostream>
#include <stdlib.h>



_UINT numAddRecords=0;

void Run() {


	SYS::Initialize();
	DB::Initialize();
	srand(time(NULL));
	SYS::MASK_REQUEST MaskRequest;
	while (numAddRecords) {
		MaskRequest.RandomGenerate(100000000);
		DB::TEST_RET TestRet = DB::Test(MaskRequest.Id_,MaskRequest.PhoneNo_,MaskRequest.Birthday_,MaskRequest.DistributorId_);
		if ( TestRet.State_ == DB::TEST_RET::Accept ) {
			_OBJ<IMem> Mem = MaskRequest.Serialize();
			_STRING MemStr((char *)Mem->Ptr(),Mem->DataLen());
			cout << MaskRequest.Id_ << " " << SYS::Base64_Encode(MemStr) << endl;
			--numAddRecords;
		}
	}
	DB::Shutdown();
	GCD::Shutdown();
}



void Shutdown() {

	SYS::Shutdown();
}



int main(int argc,char *argv[]) {
	if ( argc != 2 || (numAddRecords=atoi(argv[1])) == 0) {
		cout << "DbGen [number of records]" << endl;
	}
	else {
		GCD::Run([]{Run();},[]{Shutdown();});
	}
	return 0;
}


